import java.util.Scanner;

public class Main
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Terminal de Comandos");
        System.out.println("Intro un comando o escriba 'exit' para salir.");
        String command;
        do
        {
            command = scanner.nextLine();

            String[] comandoArray = command.split(" ");

            switch (comandoArray[0])
            {
                case "help":
                    Terminal.mostrarAyuda();
                    break;
                case "cd":
                    Terminal.cambiarRuta(comandoArray);
                    break;
                case "mkdir":
                    Terminal.crearDirectorio(comandoArray);
                    break;
                case "info":
                    Terminal.mostrarInfo(comandoArray);
                    break;
                case "cat":
                    Terminal.mostrarDatosFichero(comandoArray);
                    break;
                case "top":
                    Terminal.mostrarLineas(comandoArray);
                    break;
                case "mkfile":
                    Terminal.crearFicheroContenido(comandoArray);
                    break;
                case "write":
                    Terminal.anadirTexto(comandoArray);
                    break;
                case "dir":
                    Terminal.listarArchivosDirectorio(comandoArray);
                    break;
                case "readpoint":
                    Terminal.leerArchivoDesde(comandoArray);
                    break;
                case "delete":
                    Terminal.borrarArchivoDirectorio(comandoArray);
                    break;
                case "start":
                    Terminal.empezarProceso(comandoArray);
                    break;
                case "close":
                    Terminal.cerrarProceso(comandoArray);
                    break;
                case "clear":
                    Terminal.limpiarConsola();
                    break;
                default:
                    System.out.println("Comando no reconocido. Escribe 'help' para ver la lista de comandos disponibles.");
            }
        }
        while (!command.equals("exit"));
        scanner.close();
    }
}