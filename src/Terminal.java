import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Terminal
{
    private static String ruta = System.getProperty("user.dir");
    private static List<String> fileSystem = new ArrayList<>();
    Scanner scanner = new Scanner(System.in);

     static Path directorioActual = Path.of(ruta);


    public static void mostrarAyuda()
    {
        System.out.println("Comandos:");
        System.out.println("1. help - Lista todos los comandos.");
        System.out.println("2. cd - Muestra la ruta del directorio actual.");
        System.out.println("   - cd .. : Se mueve al directorio padre.");
        System.out.println("   - cd + nombre: Te introduce en ese directorio.");
        System.out.println("3. mkdir - Crea un directorio en la ruta actual.");
        System.out.println("4. info <nombre> - Muestra información del elemento indicando:");
        System.out.println("   - Número de elementos.");
        System.out.println("   - Espacio libre.");
        System.out.println("   - Espacio total.");
        System.out.println("   - Espacio utilizable.");
        System.out.println("5. cat <nombreFichero> - Muestra el contenido de un fichero.");
        System.out.println("6. top <numeroLineas> <nombreFichero> - Muestra las líneas especificadas de un fichero.");
        System.out.println("7. mkfile <nombreFichero> <texto> - Crea un fichero con ese nombre y el contenido de texto.");
        System.out.println("8. write <nombreFichero> <texto> - Añade 'texto' al final del fichero especificado.");
        System.out.println("9. dir - Lista los archivos o directorios de la ruta actual.");
        System.out.println("10. readpoint <nombreFichero1> <posición> - Lee un archivo desde una determinada posición del puntero.");
        System.out.println("11. delete <nombre> - Borra el fichero; si es un directorio, borra todo su contenido y a sí mismo.");
        System.out.println("12. start <programa> - Ejecuta este proceso.");
        System.out.println("13. close - Cierra el programa.");
        System.out.println("14. clear - Vacía la lista.");
    }

    public static void cambiarRuta(String[] posicion)
    {

        if (posicion.length==1)
        {
            System.out.println("Directorio actual: " + ruta);
        }
        else if (posicion[1].equalsIgnoreCase(".."))
        {

            ruta = directorioActual.getParent().toString();
            directorioActual = Path.of(ruta);
            String nuevoDirectorio;
            nuevoDirectorio= directorioActual.toString();
            System.out.println(ruta);
            ruta=nuevoDirectorio;
            String rutaActual= System.getProperty("ruta");

        }
        else if(!posicion[1].contains("\\"))
        {
            String nuevaRuta = ruta+"\\"+posicion[1];
            for (int i = 2; i<posicion.length; i++){
                nuevaRuta = nuevaRuta + " "+ posicion[i];
            }
            Path path = Path.of(nuevaRuta);
            if (Files.exists(path))
            {
                ruta = nuevaRuta;
                directorioActual = Path.of(ruta);
                System.out.println(ruta);
            }
            else
            {
                System.err.println("El archivo " + posicion[1] + " no existe en la ruta actual");
            }
        }
        else if (posicion[1].contains("\\"))
        {
            String nuevaRuta = posicion[0];
            for (int i = 1; i<posicion.length; i++){
                nuevaRuta = nuevaRuta + " "+ posicion[i];
            }

            Path path = Path.of(nuevaRuta);
            if (Files.exists(path))
            {
                ruta = nuevaRuta;
                directorioActual = Path.of(ruta);
                System.out.println(ruta);
            }
            else
            {
                System.err.println("El archivo " + posicion[1] + " no existe en la ruta actual");
            }
        }
        else
        {
            System.out.println("Estás en la raíz del sistema de archivos. No puedes retroceder más.");
        }
    }

    public static void crearDirectorio(String[] posicion)
    {
        if (posicion.length==1)
        {
            System.err.println("Falta introducir el nombre de la carpeta");
        }
        for (int i = 1; i < posicion.length; i++)
        {
            File directorio =new File(ruta + "\\"+posicion[1]);
            if (!directorio.exists())
            {
                if (directorio.mkdirs())
                {
                    System.out.println("Directorio creado correctamente en " + ruta);
                    System.out.println();
                }
                else
                {
                    System.out.println("Error al crear el directorio");
                }
            }
            else
            {
                System.err.println("El directorio ya existe");
            }
        }
    }

    public static void mostrarInfo(String[] posicion)
    {
       if (posicion.length!=2)
       {
           System.err.println("Numero de args no valido");
       }
       else
       {
           File archivo = new File(ruta+"//"+posicion[posicion.length-1]);
           if (archivo.exists())
           {
               System.out.println("Nombre de Archivo: " + archivo.getName());
               System.out.println("Absolute path: " + archivo.getAbsolutePath());
               System.out.println("Permisos de escritura: " + archivo.canWrite());
               System.out.println("Permisos de lectura: " + archivo.canRead());
               System.out.println("Tamaño del archivo: " + archivo.length());
               System.out.println("Carpeta contenedora: " + archivo.getParent());
               System.out.println("Directorio: " + archivo.isDirectory());
               System.out.println("Archivo: " + archivo.isFile());
               System.out.println("Oculto: " + archivo.isHidden());
           }
           else
           {
               System.out.println("El archivo no existe");
           }
       }
    }

    public static void mostrarDatosFichero(String[] posicion)
    {
        if(posicion.length!=2)
        {
            System.err.println("Numero de args no valido");
        }
        else
        {
          String nuevaRuta=ruta+"\\"+posicion[1];
          File fichero =new File(nuevaRuta);

          try (BufferedReader br = new BufferedReader(new FileReader(nuevaRuta)))
          {
              String linea;
              while ((linea = br.readLine()) != null)
              {
                  System.out.println(linea);
              }
          }
          catch (IOException e)
          {
              System.out.println("El archivo no existe");
          }
        }
    }

    public static void mostrarLineas(String[] posicion)
    {
        if (posicion.length != 3)
        {
            System.err.println("Numero de args no valido");
        }
        else
        {
            String nuevaRuta = ruta + "\\" + posicion[2];
            File fichero = new File(nuevaRuta);

            try
            {
                int numero = Integer.parseInt(posicion[1]);
                int numeroLineaQueQueremos = numero;
                try (BufferedReader br = new BufferedReader(new FileReader(nuevaRuta)))
                {
                    int numeroLinea = 1;
                    String linea;
                    while ((linea =br.readLine())!=null)
                    {
                        if (numeroLineaQueQueremos == numeroLinea)
                        {
                            System.out.println(linea);
                        }
                        numeroLinea++;
                    }
                }
                catch (NumberFormatException e)
                {
                    System.err.println("La cadena no es valida");
                }
            }
            catch (IOException e)
            {
                System.err.println("El archivo no existe");
            }
        }
    }


    public static void crearFicheroContenido(String[] posicion)
    {
        if (posicion.length==1)
        {
            System.err.println("Debes introducir el nombre al archivo");
        }
        for (int i=1; i<posicion.length; i++)
        {
            String nuevaRuta =ruta+"\\"+posicion[i];
            try
            {
                File fichero = new File(nuevaRuta);
                if (fichero.exists())
                {
                    System.err.println("El fichero ya existe");
                }
                else
                {
                    fichero.createNewFile();
                    System.out.println("Fichero creado");
                }
            }
            catch (IOException e)
            {
                System.err.println("No se ha podido crear el fichero");
            }
        }
    }

    public static void anadirTexto(String[] posicion)
    {
        if (posicion.length==1)
        {
            System.out.println("Numero de args no valido");
        }
        else
        {
            if (!posicion[1].endsWith(".txt"))
            {
                System.err.println("El archivo debe tener la extension .txt.");
            }
            String nuevaRuta = ruta+"\\"+posicion[1];
            File fichero =new File(nuevaRuta);
            String textoEscribir=" ";
            if(fichero.exists()){
                for (int i=2; i<posicion.length; i++)
                {
                    textoEscribir=textoEscribir+" "+posicion[1];
                }
                try(BufferedWriter bw=new BufferedWriter(new FileWriter(fichero,true)))
                {
                    bw.write(textoEscribir);
                    bw.newLine();
                    System.out.println("Texto escrito correctamente");
                }
                catch (IOException e)
                {
                    System.err.println("El archivo no existe");
                    e.printStackTrace();
                }
            }
            else
            {
                System.err.println("El archivo no existe");
            }
        }
    }

    public static void listarArchivosDirectorio(String[] posicion )
    {
        {
            if (posicion.length !=1)
            {
                System.err.println("Numero de args no valido");
            }
        try
        {
            String directorioActual = ruta;

            System.out.println("Contenido de "+directorioActual +":");
            listarContenidoDirectorio(directorioActual,0);
        }
        catch (Exception e)
            {
                System.out.println("Error: "+ e.getMessage());
            }
        }
    }

    public static void listarContenidoDirectorio(String ruta,int nivel)
    {
        try
        {
            File carpeta =new File(ruta);
            File[] archivos=carpeta.listFiles();
            if(archivos !=null)
            {
                for(File archivo : archivos)
                {
                    String Espacios= " ".repeat(nivel);
                    System.out.println(Espacios + archivo.getName() + (archivo.isDirectory()? "/" : ""));
                    if (archivo.isDirectory())
                    {
                        listarContenidoDirectorio(archivo.getAbsolutePath(), nivel + 1);
                    }
                }
            }
        }
        catch (Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }
    }
    public static void leerArchivoDesde(String[] posicion)
    {
        if (posicion.length !=3)
        {
            System.err.println("Numero de args no valido");
        }
        try
        {
          String nomFichero = posicion[1];
          long lugar =Long.parseLong(posicion[2]);
          try (RandomAccessFile archivo=new RandomAccessFile(nomFichero,"r"))
          {
            archivo.seek(lugar);

            int leido;
            while ((leido = archivo.read()) != -1)
            {
                System.out.println((char)leido);
            }
          }
          catch (IOException e)
          {
              System.err.println("El archivo no existe");
          }
        }
        catch(NumberFormatException e)
        {
            System.err.println("La cadena no es valida");
        }
    }

    public static void borrarArchivoDirectorio(String[] posicion)
    {
        if (posicion.length != 2)
        {
            System.out.println("Número de argumentos no válido");
            return;
        }

        String nombre = ruta + "\\" + posicion[1];
        File archDir = new File(nombre);

        if (!archDir.exists())
        {
            System.err.println("El archivo o directorio no existe");
            return;
        }

        if (archDir.isDirectory())
        {
            boolean borradoExitoso = eliminarDirectorio(archDir);
            if (borradoExitoso)
            {
                System.out.println("El directorio y su contenido se han borrado exitosamente");
            }
            else
            {
                System.err.println("No se pudo borrar el directorio y su contenido");
            }
        }
        else
        {
            boolean borradoExitoso = archDir.delete();
            if (borradoExitoso)
            {
                System.out.println("El archivo se ha borrado exitosamente");
            }
            else
            {
                System.err.println("No se pudo borrar el archivo");
            }
        }
    }

    private static boolean eliminarDirectorio(File directorio)
    {
        if (directorio.isDirectory())
        {
            File[] archivos = directorio.listFiles();
            if (archivos != null)
            {
                for (File archivo : archivos)
                {
                    if (archivo.isDirectory())
                    {
                        eliminarDirectorio(archivo);
                    }
                    else
                    {
                        archivo.delete();
                    }
                }
            }
        }
        return directorio.delete();
    }

    public static void empezarProceso(String[] posicion)
    {
       if (posicion.length!=2)
       {
           System.err.println("Numero de args no valido");
       }
       String programa = posicion[1];;
       ProcessBuilder pb = new ProcessBuilder(programa);

       try
       {
           Process proceso =pb.start();
       }
       catch (IOException e)
       {
           System.err.println("El proceso no existe");
       }
    }

    public static void cerrarProceso(String[] posicion)
    {
        System.exit(0);
    }

    public static void limpiarConsola()
    {
        for (int i = 0; i <50;i++)
        {
            System.out.println();
        }
        System.out.println("Terminal de Comandos");
        System.out.println("Intro un comando o escriba 'exit' para salir.");
    }
}